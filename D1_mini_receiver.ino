#include <ESP8266WiFi.h>

//TODO:
// timeout handling

#define STEP_PIN    D7
#define DIR_PIN     D8
#define STEP_DELAY 800
#define MAX_TIMEOUT 20

IPAddress server(192,168,4,1);
WiFiClient myClient;

int pins[9]={BUILTIN_LED,D1,D2,D3,D4,D5,D6,D7,D8};
//pins used:
//  D5 = error LED pin
//  D6 = info LED pin
//  D7 = step pin
//  D8 = dir pin
boolean dir = 0;
boolean errorOccured = false;

void setup() {

    //Serial.begin(9600);
    pinsSetup();
    wifiSetup();
    tellBaseYouReady();
    if(waitForOrderFromBase()){
        SingleTurn();
        sendBaseFeedback(true);
    } else {
        sendBaseFeedback(false);
    }
    if(errorOccured){
        //maybe later for some debuging
    }
    ESP.deepSleep(0);
  
}

void loop() {
    
}


//----------------CUSTOM FUNCTIONS ---------------------------------------------------------------------------------

//sends message to base to let it know the module is read --------------------------------------------------------------------------------
void tellBaseYouReady(){
    if(!errorOccured){
        if(myClient.connect(server,80)){
            myClient.print("awake\r");
        } else {
            errorOccured = true;
            digitalWrite(pins[6],LOW);
            digitalWrite(pins[5],HIGH);
            delay(500);
        }
    }
}

//waits for orders from base after wake up (should happend almost instainteniously, waits the amount of time built into .readStringUntil()
boolean waitForOrderFromBase(){
    if(!errorOccured){
        String order = "";
        int timeoutCounter = 0;
        do{
            order = myClient.readStringUntil('\r');
            timeoutCounter++;
            delay(50);
        }while( !order.equals("close") &&  !order.equals("open") && timeoutCounter <= 40);
        
        if(timeoutCounter >= 40){
            digitalWrite(pins[5],HIGH);
            delay(1000);
            digitalWrite(pins[5],LOW);
            delay(1000);
        }

        
        if(order.equals("close")){
            digitalWrite(DIR_PIN,HIGH);
            return true;
        } else if (order.equals("open")) {
            digitalWrite(DIR_PIN,LOW);
            return true;
        } else {
            for(int i =0; i<5; i++){
                digitalWrite(pins[5],HIGH);
                delay(500);
                digitalWrite(pins[5],LOW);
                delay(500);
            }
            return false;
        }
        
    } else {
        return false;
    }
}

//send feedback mssage to base
void sendBaseFeedback(boolean wasSuccessful){
    if(!errorOccured){
        if(wasSuccessful){
            myClient.print("success\r");
        } else {
            myClient.print("failed\r");
        }
        myClient.flush();
        myClient.stop();
    }
}

//performs a single step of the motor --------------------------------------------------------------------------------------------------------
void SingleStep(){
    digitalWrite(STEP_PIN,HIGH); 
    delayMicroseconds(STEP_DELAY); //above 1000
    digitalWrite(STEP_PIN,LOW); 
    delayMicroseconds(STEP_DELAY); 
}

//performs one full turn of the motor ------------------------------------------------------------------------------------------------------
void SingleTurn(){
    for(int i=0; i<400; i++){
        SingleStep();
    }
}

//setup pins as inputs or outputs------------------------------------------------------
//verified
void pinsSetup(){
    for(int i=0;i<9;i++){
        pinMode(pins[i],OUTPUT);
        digitalWrite(pins[i],0);
    }
}

//connect module to AP created by base module -----------------------------------------------------------------
//verified
void wifiSetup(){
    WiFi.mode(WIFI_STA);
    WiFi.begin("ESPsoftAP_01", "2niFa1tSCVQtaK6FkaykjADb3MvUjrj4MqAPx9tdpL8DSg5mCRWHec2sdZ0bIFu");

    int timeoutCounter = 0;
    while(WiFi.status() != WL_CONNECTED && timeoutCounter < MAX_TIMEOUT) {
        digitalWrite(pins[0],HIGH);
        delay(100);
        digitalWrite(pins[0],LOW);
        delay(100);
        timeoutCounter++;
    }
    if(timeoutCounter >= MAX_TIMEOUT){
        errorOccured = true;
        digitalWrite(pins[5],HIGH);
        delay(500);
    } else{
        digitalWrite(pins[6],HIGH);
    }
}
