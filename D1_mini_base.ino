#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

#define MAX_WAIT 10000


int pins[9]={D0,D1,D2,D3,D4,D5,D6,D7,D8};
String webserverMSG = "";
//pins used:
//  D3 = connection status LED
//  D7 = button input
//  D6 = interrupt signal output

String order = "";
String WiFiIP = "";

WiFiClient myClient;

// Create a new server
WiFiServer server(80);
ESP8266WebServer webserver(80);

void setup(){
    Serial.begin(9600);
    pinsSetup();
    wifiSetup();
}

void loop(){
    webserver.handleClient(); 
}


//----------------CUSTOM FUNCTIONS ------------------------------------------------------------
//function sends impulse waking up the working station-----------------------------------------
void pingStation(){
    digitalWrite(pins[6],HIGH);
    delay(1);
    digitalWrite(pins[6],LOW);
    delay(1);
}

//function awaiting waking up of working station-----------------------------------------------
boolean waitForStationToWakeUp(){
    int i = 0;
    String request = "";
    do{
        delay(20);
        i+=20;
        myClient = server.available();
    }while(!myClient && i <= MAX_WAIT);
    
    if(myClient){
        do{
            request = myClient.readStringUntil('\r');
            delay(1);
        }while(request.equals(""));
    } else {
        return false;
    }
    
    if(request.equals("awake")){
        return true;
    } else{
        return false;
    }
}

//function sending order to working station--------------------------------------------------
void sendOrder(){
    myClient.println(order + "\r");
}

//function waiting for feedback from working station-----------------------------------------
void waitForStationToPerformAction(){
    delay(700);
    String feedback = myClient.readStringUntil('\r');
    if(feedback.equals("success")){
        Serial.println("successfuly performed action");
    } else if(feedback.equals("failed")){
        Serial.println("action failed");
    } else {
        Serial.println("something went wrong");
    }
    webserverMSG = webserverMSG + feedback;
    myClient.flush();
    myClient.stop();
    
}

//callback function for the root page----------------------------------------------------------
void rootPage(){

    webserver.send(200, "text/plain", "No cześć :)"); 
    
}

//callback function for /open request----------------------------------------------------------
void openDoor(){
    Serial.println("received request");
    webserverMSG = "this function is gonna open the door; ";
    digitalWrite(pins[3],0);
    for(int i=0; i<3; i++){
        digitalWrite(pins[3],1);
        delay(50);
        digitalWrite(pins[3],0);
        delay(50);
    }
    digitalWrite(pins[3],1);
    order = "open";
    pingStation();
    if(waitForStationToWakeUp()){
        Serial.println("found a client");
        webserverMSG = webserverMSG + "found a client; ";
        sendOrder();
        waitForStationToPerformAction();
    } else {
        Serial.println("didn't find a client");
        webserverMSG = webserverMSG + "didn't find a client; ";
    }
    order = "";
    webserver.send(200, "text/plain", webserverMSG); 
}


//callback function for /close request---------------------------------------------------------
void closeDoor(){
    Serial.println("received request");
    webserverMSG = "this function is gonna close the door; ";
    digitalWrite(pins[3],0);
    for(int i=0; i<3; i++){
        digitalWrite(pins[3],1);
        delay(50);
        digitalWrite(pins[3],0);
        delay(50);
    }
    digitalWrite(pins[3],1);
    order = "close";
    pingStation();
    if(waitForStationToWakeUp()){
        Serial.println("found a client");
        webserverMSG = webserverMSG + "found a client";
        sendOrder();
        waitForStationToPerformAction();
    } else {
        Serial.println("didn't find a client");
        webserverMSG = webserverMSG + "didn't find a client"; 
    }
    order = "";
    webserver.send(200, "text/plain", webserverMSG); 
}


// Handle 404
void notfoundPage(){ 
  webserver.send(404, "text/plain", "404: Not found");
}


//setup pins as inputs or outputs----------------------------------------------------------
void pinsSetup(){
    for(int i=0;i<9;i++){
        pinMode(pins[i],OUTPUT);
        digitalWrite(pins[i],0);
    }
    pinMode(pins[7],INPUT);
}

//connect module to wifi-------------------------------------------------------------------
void wifiSetup(){

    delay(1000);
    Serial.println();
    Serial.println("Waiting for WPS to be activated, press WPS button on your router and then the button on the station");
    while(digitalRead(pins[7]) == HIGH){
        digitalWrite(pins[3],1);
        delay(100);
        digitalWrite(pins[3],0);
        delay(100);
    }
    digitalWrite(pins[3],0);
    
  // Connect to WiFi network
    WiFi.mode(WIFI_STA);
    Serial.print("Begin WPS ... ");
    if(WiFi.beginWPSConfig()){
        Serial.print("Success");
        digitalWrite(pins[3],1);
        delay(100);
        digitalWrite(pins[3],0);
        delay(100);
        digitalWrite(pins[3],1);
        delay(100);
        digitalWrite(pins[3],0);
        delay(100);
    } else {
        Serial.print("Failed");
        digitalWrite(pins[3],1);
        delay(100);
        digitalWrite(pins[3],0);
    }
    digitalWrite(pins[3],1);
    
    //Configure module as an access point
    WiFi.mode(WIFI_AP_STA);
    Serial.print("Setting soft-AP ... ");
    Serial.println(WiFi.softAP("ESPsoftAP_01", "2niFa1tSCVQtaK6FkaykjADb3MvUjrj4MqAPx9tdpL8DSg5mCRWHec2sdZ0bIFu", 1, true, 1) ? "Ready" : "Failed!");
    server.begin();
    Serial.println("Server started");
    Serial.println("IP:");
    Serial.println(WiFi.softAPIP());
    webserver.on("/", rootPage);
    webserver.on("/open", openDoor);
    webserver.on("/close", closeDoor);
    webserver.onNotFound(notfoundPage);
    webserver.begin();
    delay(2200);
    Serial.println("\nWiFi connected with IP: ");
    Serial.println(WiFi.localIP());
    WiFiIP = String(WiFi.localIP());
    
}
